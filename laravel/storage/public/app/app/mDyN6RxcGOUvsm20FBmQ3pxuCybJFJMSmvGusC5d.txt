-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 12, 2017 at 04:08 PM
-- Server version: 5.7.19-0ubuntu0.17.04.1
-- PHP Version: 7.0.23-1+ubuntu17.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gocart_two`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner_id` int(9) UNSIGNED NOT NULL,
  `banner_collection_id` int(9) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `enable_date` date NOT NULL,
  `disable_date` date NOT NULL,
  `image` varchar(64) NOT NULL,
  `link` varchar(128) DEFAULT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `banner_collection_id`, `name`, `enable_date`, `disable_date`, `image`, `link`, `new_window`, `sequence`) VALUES
(1, 1, 'One', '2017-03-04', '2018-03-30', 'da7ebe3a20f920cda6ba2d0f45867508.png', 'http://mtn.com', 1, 0),
(2, 1, 'Two', '2017-03-04', '2017-12-30', '89f357d77b143498dacdc8956081f68e.png', 'http://irokotv.com', 1, 1),
(3, 1, 'Three', '2017-03-04', '2018-02-02', '8e8092b62dcdda6f8170fd5a431c63c9.png', 'http://hotels.ng', 0, 2),
(4, 2, 'One', '2017-03-04', '2018-03-10', '47d5caab8ede2ebf7fd56fdfd4b83899.jpg', 'http://facebook.com', 1, 0),
(5, 2, 'Two', '2017-03-04', '2017-11-24', '807215ce75b53b37220b85bd35d890b9.jpg', 'http://twitter.com/massivebrains00', 1, 1),
(6, 2, 'Three', '2017-03-04', '2018-02-10', 'f8c21332c56f6957cfcf073021ac4092.jpg', 'http://twitter.com/massivebrains00', 1, 2),
(7, 2, 'Four', '2017-03-04', '2017-10-27', 'c71a8d852c3859a305ab353248afbc6a.jpg', '#', 1, 3),
(8, 3, 'One', '2017-03-04', '2018-04-28', '0042256e9b12ca380c9d4f7f592eeb23.png', 'http://facebook.com', 1, 0),
(9, 4, 'One', '2017-03-06', '2018-03-31', 'c56bc096add787661f9a940cef3bde5c.png', 'http://twitter.com/massivebrains00', 1, 0),
(10, 4, 'Two', '2017-03-06', '2018-08-04', '37363c0763c0e167cc854e1f21ca3424.png', 'http://facebook.com', 1, 1),
(11, 4, 'Three', '2017-03-06', '2018-06-09', 'ae194efc9f6c055860e6c1b76df1264f.png', 'http://tft-spark.co/shopmoni', 1, 2),
(12, 5, 'One', '2017-03-08', '2018-03-24', '4296ae47e870373108af80ee5d3ff1cf.png', 'http://tft-spark.co/shopmoni/product/nokia-x-dual-sim-dual-core-smartphone-green', 1, 0),
(13, 6, 'One', '2017-04-08', '2017-11-11', 'c34d5aab7d13cd678e39a069fa875ea4.png', 'http://tft-spark.co/shopmoni', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(11) NOT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `sender_code` varchar(255) NOT NULL,
  `recipient_code` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `sender_name`, `sender_email`, `customer_id`, `sender_code`, `recipient_code`, `created_at`) VALUES
(1, 'Daniel', 'kayode@gmail.com', 7, 'bPQ2TCDNAzlHiVSL', 'rbHt9yS1ixu4Zpqc', '2017-10-12 15:00:12'),
(2, 'Daniel', 'daniel@gmail.com', 7, 'ATtlZyuSXG4kWKsJ', 'aRjuEpe2gvT3I7NJ', '2017-10-12 15:02:29'),
(3, 'Sonny', 'sonny@gmail.com', 7, 'CqB3mDRX4Vf5Zsce', 'G29bQxTL4vjYNRPe', '2017-10-12 15:04:58');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `sender` varchar(50) DEFAULT NULL,
  `message` text,
  `read_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `conversation_id`, `sender`, `message`, `read_at`, `created_at`) VALUES
(1, 1, 'rbHt9yS1ixu4Zpqc', 'Hi daniel..', NULL, '2017-10-12 15:00:31'),
(2, 2, 'ATtlZyuSXG4kWKsJ', 'Oh ok', NULL, '2017-10-12 15:02:39'),
(3, 2, 'aRjuEpe2gvT3I7NJ', 'Mayb this workds..', NULL, '2017-10-12 15:03:09'),
(4, 2, 'ATtlZyuSXG4kWKsJ', '\nYea it does wok', NULL, '2017-10-12 15:03:19'),
(5, 2, 'aRjuEpe2gvT3I7NJ', 'Im moving this crap right away', NULL, '2017-10-12 15:03:31'),
(6, 2, 'ATtlZyuSXG4kWKsJ', '\nOk Thanks', NULL, '2017-10-12 15:03:48'),
(7, 3, 'G29bQxTL4vjYNRPe', 'Hi sonny', NULL, '2017-10-12 15:05:29'),
(8, 3, 'CqB3mDRX4Vf5Zsce', 'hi', NULL, '2017-10-12 15:05:38'),
(9, 2, 'aRjuEpe2gvT3I7NJ', 'Still here..', NULL, '2017-10-12 15:05:54'),
(10, 2, 'ATtlZyuSXG4kWKsJ', '\nhmm', NULL, '2017-10-12 15:06:02'),
(11, 2, 'aRjuEpe2gvT3I7NJ', 'ok', NULL, '2017-10-12 15:06:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banner_id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
