<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->call('App\Http\Controllers\Cron\MessageController@custom')->everyMinute();
        $schedule->call('App\Http\Controllers\Cron\MessageController@customized')->everyMinute();
        $schedule->call('App\Http\Controllers\Cron\MessageController@send')->everyMinute();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
