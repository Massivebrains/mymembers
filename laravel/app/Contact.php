<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
	use SoftDeletes;

    protected $guarded = ['deleted_at'];
    protected $dates = ['deleted_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function groups()
    {
    	return $this->belongsToMany('App\Group', 'contact_group');
    }

    public function age_bracket()
    {
        return $this->belongsTo('App\AgeBracket');
    }
}
