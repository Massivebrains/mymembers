<?php

namespace App\Http\Controllers\Contacts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Contact;
use App\User;
use App\AgeBracket;
use App\Group;
use DB;
use Auth;
use Excel;
use Validator;
use Illuminate\Validation\Rule;
use App\Rules\PhoneNumberValidationUpload;

class ContactsUploadController extends Controller
{
    public function index()
    {
        $data['age_brackets'] = User::find(_id())->age_brackets;
        $data['groups']       = User::find(_id())->groups;
        $data['file_url']     = Storage::url('template.xlsx');
        
        return view('contacts.upload_guide', $data);
    }

    public function store(Request $request)
    {
        if($request->has('upload')){

            $this->validate($request, ['contacts' => 'mimes:xlsx,csv']);

            $path = $request->file('contacts')->store('uploads');

            //dd($path);
            $data = Excel::load(public_path($path), function($reader){


            })->get();

            $errorBag   = [];
            $errorRows  = [];
            $i          = 0;
            $errorCount  = $successCount = 0;
            $date       = date('Y-m-d H:i:s');

            foreach($data as $row){

                $payload = [

                    'firstname'         => strtolower(isset($row->firstname) ? $row->firstname : ''),
                    'surname'           => strtolower(isset($row->surname) ? $row->surname : ''),
                    'phone'             => strtolower(isset($row->phone) ? $row->phone : ''),
                    'email'             => strtolower(isset($row->email) ? $row->email : ''),
                    'birth_month'       => strtolower(isset($row->birth_month) ? $row->birth_month : ''),
                    'birth_day'         => strtolower(isset($row->birth_day) ? $row->birth_day : ''),
                    'age_bracket'       => strtolower(isset($row->age_bracket) ? $row->age_bracket : ''),
                    'language'          => strtolower(isset($row->language) ? $row->language : ''),
                    'gender'            => strtolower(isset($row->gender) ? $row->gender : ''),
                ];


                $validator = Validator::make($payload, [

                    'firstname'         => 'required|alpha',
                    'surname'           => 'required|alpha',
                    'phone'             => ['required', 'digits_between:10,11', new PhoneNumberValidationUpload],
                    'email'             => 'email',
                    'birth_month'       => 'required|numeric|max:12',
                    'birth_day'         => 'required|numeric|max:31',
                    'age_bracket'       =>  'required',
                    'language'          => ['required', Rule::in('english', 'yoruba', 'igbo', 'hausa')],
                    'gender'            => ['required', Rule::in('male', 'female')]
                ]);

                if($validator->fails()){

                    $errorBag[$i]   = '';
                    $errorRows[$i]  = $row;

                    foreach($validator->errors()->all() as $error){

                        $errorBag[$i] .= $error.'<br>';
                    }

                     $errorCount++;

                }else{

                    
                    $ageBracket =  AgeBracket::firstOrNew([

                        'user_id' => _id(), 
                        'name' => $row->age_bracket,
                        'deleted_at' => null

                    ]);

                    $contact = Contact::create([

                        'user_id'       => _id(),
                        'firstname'     => $row->firstname,
                        'lastname'      => $row->surname,
                        'phone'         => config('settings.nigeria_prefix').substr($row->phone, -10),
                        'email'         => $row->email,
                        'month'         => $row->birth_month,
                        'day'           => $row->birth_day,
                        'age_bracket_id' => $ageBracket->id,
                        'language'      => $row->language,
                        'gender'        => $row->gender,
                        'created_at'    => $date

                    ]);


                    $groups = explode(',', $row->groups);

                    foreach($groups as $group){

                        $_group = Group::firstOrNew([

                            'user_id' => _id(), 
                            'name' => trim($group),
                            'deleted_at' => null
                        ]);

                        if($_group->id > 0){

                            DB::table('contact_group')->insert([

                                'contact_id' => $contact->id,
                                'group_id' => $_group->id
                            ]);
                        }
                    }

                    $successCount++;

                }

                $i++;
            }

            $response = [

                'errors' => $errorBag,
                'error_rows' => $errorRows,
                'error_count' => $errorCount,
                'success_count' => $successCount
            ];

             _log('contact Data Uploaded. [Total Saved : '.$successCount.']');
                
            return view('contacts.upload_result', $response)->with('status', 'Upload was succesful. '.$successCount.' of '.$errorCount+$successCount.' was succesfully Saved.');
        }
    }
}
