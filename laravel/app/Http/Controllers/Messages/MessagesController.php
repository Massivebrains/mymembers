<?php

namespace App\Http\Controllers\Messages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Message;
use App\Contact;

class MessagesController extends Controller
{
	public function index()
	{
		$data['messages'] = Message::where(['user_id' => _id()])->orderBy('created_at', 'DESC')->get();

		return view('messages.index', $data);
	}

	public function birthdays()
	{	
		$i = 0;

		foreach(_months() as $index => $value){

			$birthdays[$i]['month'] 			= $value;
			$birthdays[$i]['birthdays']			= Contact::where(['user_id' => _id(), 'month' => $index])->count();
			$birthdays[$i]['celebrated']		= Contact::where(['user_id' => _id(), 'month' => $index])->where('day', '>', $index)->count();

			$i++;
		}

		return view('messages.birthdays', ['birthdays' => $birthdays]);
	}
}
