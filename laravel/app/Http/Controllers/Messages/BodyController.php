<?php

namespace App\Http\Controllers\Messages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class BodyController extends Controller
{
    public function index(Request $request, $id = NULL)
    {
        if($request->isMethod('post')){

            $this->validate($request, [ 'body' => 'required' ]);

            $message = Message::updateOrCreate(['id' => $id], [

                'user_id'       => _id(),
                'body'          => request('body'),
                'translate'     => (bool)request('translate')

            ]);

            return redirect('message/recipients/'.$message->id.'/'.md5(time()));

        }
        
        return view('messages.new.body', ['message' => Message::firstOrNew(['id' => $id])]);
    }
}
