<?php

namespace App\Http\Controllers\Messages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use App\User;
use Auth;
use DB;

class OptionsController extends Controller
{
    public function index(Request $request, $id = null)
    {
        $message = Message::where(['id'=> $id])->first();

        if($message != null && $message->user->id == _id()){

            return view('messages.new.options', ['message_id' => $id]);
        }
        
        abort(404);
    }

    public function save(Request $request)
    {        
        $message = Message::where(['id' => request('message_id')])->first();

        if($message == null){

            abort(404);
        }

        Message::where(['id' => $message->id])->update([

            'schedule_date'     => date('Y-m-d', strtotime(request('schedule_date'))),
            'schedule_time'     => date('H:i:s', strtotime(request('schedule_time'))),
            'repitition_type'   => request('repitition_type'),
            'repitition_value'  => request(request('repitition_type').'_repitition')
        ]);

        return redirect('message/review/'.$message->id.'/'.md5(time()));

    }
}
