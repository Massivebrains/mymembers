<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Recipient;
use App\GoogleTranslate;
use App\Message;
use App\User;
use DB;

class MessageController extends Controller
{

	public function custom()
	{
		$message = Message::where([

			'status' 			=> 'queued', 
			'recipients_type' 	=> 'custom'

		])->first();

		if($message != null){

			$recipients = Recipient::recipients($message->id);

			foreach($recipients as $row){

				DB::table('sms_bank')->insert([

					'message_id'		=> $message->id,
					'phone' 			=> $row->phone,
					'sms'				=> $message->body,
					'created_at' 		=> date('Y-m-d H:i:s')

				]);
			}

			Message::where(['id' => $message->id])->update(['status' => 'completed']);
		}


	}


	public function customized()
	{
		
		$message = Message::where(['status' => 'queued'])->where('recipients_type', '!=', 'custom')->first();

		if($message != null){

			$recipients = Recipient::recipients($message->id);

			foreach($recipients as $row){

				$body = $message->body;

				if(strripos($message->body, '#name#')){

					$body = str_replace('#name#', $row->firstname, $message->body);
				}

				if($message->translate == 1 && $row->language != 'english'){

					$body = GoogleTranslate::translate($body, $row->language);
				}

				DB::table('sms_bank')->insert([

					'message_id'	=> $message->id,
					'phone' 		=> $row->phone,
					'sms'			=> $body,
					'created_at' 	=> date('Y-m-d H:i:s')
				]);
			}

			Message::where(['id' => $message->id])->update(['status' => 'completed']);
		}

	}

	public function send()
	{
		$tenMessages = DB::table('sms_bank')->where(['status' => 'queued'])->limit(10)->get();

		foreach($tenMessages as $row){

			$message = Message::where(['id' => $row->message_id])->first();

			$settings = DB::table('settings')->where(['user_id' => $message->user_id])->first();

			$source = 'STRIBE';

			if($settings != null){

				$source = strtoupper(substr($settings->sender_name, 0, 11));
			}

			$endpoint = 	"http://smsplus4.routesms.com/bulksms/bulksms?username=escape1&password=z9m2w4x7";
			$endpoint.= 	"&type=0&destination={$row->phone}";
			$endpoint.=		"&source={$source}";
			$endpoint.=		"&message={$row->sms}";
			
			$response = '';

			try{

				(new \GuzzleHttp\Client())->request('GET', $endpoint);


				DB::table('sms_bank')->where(['id' => $row->id])->update([

					'status' 		=> 'sent',
					'updated_at'	=> date('Y-m-d H:i:s')
				]);

				$response = 'successful';
				
			}catch(Exception $e){

				$response = $e->getMessage();

			}finally{

				DB::table('sms_bank')->where(['id' => $row->id])->update(['response' => $response]);
			}
			

		}
	}
}
