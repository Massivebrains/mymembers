<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Contact;

class HomeController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $contacts = Contact::where(['user_id' => _id()])->get();

        $i          = 0;
        $birthdays  = [];
        
        foreach($contacts as $row){

            $birthdays[$i]['title']      = $row->firstname.' '.$row->lastname;
            $birthdays[$i]['start']      = date('Y').'-'.$row->month.'-'.$row->day;

            $i++;
        }

        $data['sent']       = Message::where(['user_id' => _id(), 'status' => 'completed'])->count();
        $data['queued']     = Message::where(['user_id' => _id(), 'status' => 'queued'])->count();
        $data['contacts']    = Contact::where(['user_id' => _id()])->count();
        $data['birthdays']  = $birthdays;

        return view('dashboard.index', $data);
    }

    public function helpdesk()
    {
        return view('dashboard.helpdesk');
    }
}
