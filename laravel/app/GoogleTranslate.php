<?php

namespace App;

use Google\Cloud\Translate\TranslateClient;
use Storage;

class GoogleTranslate
{
    public static function translate($text = '', $targetLanguage = ''){
        
    	if($targetLanguage == 'yoruba'){

    		$code = 'yo';

    	}elseif($targetLanguage == 'igbo'){

    		$code = 'ig';

    	}elseif($targetLanguage == 'hausa'){

    		$code = 'ha';

    	}else{

    		return $text;
    	}

    	 $translate = new TranslateClient([

            'keyFilePath'   => base_path('MyProject-f88cc7542954.json'),
    	 	'projectId'     => 'arcane-stack-169311'

    	 ]);


    	 $translation = $translate->translate($text, [

    	 	'target' => $code

    	 ]);

    	 return _normalizeChars($translation['text']);
    	
    }

}
