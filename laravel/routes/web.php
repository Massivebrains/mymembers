<?php

Route::get('/', function () {

	return view('index');

});

Route::any('login', 'AuthController@login');
Route::any('register', 'AuthController@register');
Route::get('auth/verify/{token}', 'AuthController@verify');
Route::get('auth/resend', 'AuthController@resend');
Route::any('logout', 'AuthController@logout');


Route::group(['middleware' => 'auth'], function(){

	Route::get('home', 'HomeController@index');
	Route::get('settings', 'SettingsController@index');
	Route::post('settings/save', 'SettingsController@store');

	Route::get('buy', 'BuyCreditsController@index');
	Route::get('reports', 'ReportsController@index');
	Route::get('helpdesk', 'HomeController@helpdesk');

	Route::group(['namespace' => 'contacts'], function(){

		Route::get('contacts', 'ContactsController@index');
		Route::get('contact/{id?}', 'ContactsController@contact');
		Route::post('contact/{id?}', 'ContactsController@store');

		Route::get('contacts-upload-guide', 'ContactsUploadController@index');
		Route::post('contacts-upload', 'ContactsUploadController@store');

		Route::post('contacts-search', 'ContactsSearchController@index');

	});

	Route::group(['namespace' => 'messages'], function(){

		Route::any('message/new/{id?}/{random?}', 'BodyController@index');
		Route::get('message/recipients/{id?}/{random?}', 'RecipientController@index');
		Route::post('message/save-recipients', 'RecipientController@save');
		Route::get('message/options/{id?}/{random?}', 'OptionsController@index');
		Route::post('message/save-options', 'OptionsController@save');
		Route::get('message/review/{id?}/{random?}', 'ReviewController@index');
		Route::get('message/queue/{id?}', 'QueueController@index');
		Route::get('messages', 'MessagesController@index');
		Route::get('birthdays', 'MessagesController@birthdays');

	});

});

Route::group(['prefix' => 'ajax'], function(){

	Route::post('age-brackets', 'AgeBracketsController@index');
	Route::get('destory-age-bracket/{id?}', 'AgeBracketsController@destroy');

	Route::post('groups', 'GroupsController@index');
	Route::get('destory-group/{id?}', 'GroupsController@destroy');

});

Route::group(['prefix' => 'cron'], function(){

	Route::get('/queue/custom', 'Cron\MessageController@queueCustom');
	Route::get('/queue/all', 'Cron\MessageController@queueCustomizedAndAll');
	Route::get('/queue/send', 'Cron\MessageController@sendSms');
	
});

