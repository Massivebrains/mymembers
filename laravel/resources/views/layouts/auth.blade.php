<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Stribe</title>
    <meta name="description" content="">
    <meta name="author" content="Olaiya Segun">
    <meta name="robots" content="noindex, nofollow">        
    <meta property="og:title" content="">
    <meta property="og:site_name" content="mymembers.com">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">        
    <link rel="shortcut icon" href="{{asset('public/img/favicons/favicon.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('public/img/favicons/favicon-192x192.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('public/img/favicons/apple-touch-icon-180x180.png')}}">
    <link rel="stylesheet" id="css-main" href="{{asset('public/css/codebase.min.css')}}">
    <style type="text/css">
    .btn{

        cursor : pointer !important;
    }

    .btn-xs{

        padding:1px 5px;
        font-size:12px;
        line-height:1.5;
        border-radius:3px;
        height:20px;
    }
</style>
</head>
<body>
    <div id="page-container" class="main-content-boxed">

        <main id="main-container">

            @yield('content')

        </main>

    </div>

    <script src="{{asset('public/js/core/jquery.min.js')}}"></script>
    <script src="{{asset('public/js/core/popper.min.js')}}"></script>
    <script src="{{asset('public/js/core/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/js/core/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('public/js/core/jquery.scrollLock.min.js')}}"></script>
    <script src="{{asset('public/js/core/jquery.appear.min.js')}}"></script>
    <script src="{{asset('public/js/codebase.js')}}"></script>
    <script src="{{asset('public/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

</body>
</html>