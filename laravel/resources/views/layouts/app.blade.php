<!doctype html>
<html lang="{{app()->getLocale()}}" class="no-focus"> 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Stribe</title>
    <meta name="description" content="">
    <meta name="author" content="Olaiya Segun">
    <meta name="robots" content="noindex, nofollow">        
    <meta property="og:title" content="">
    <meta property="og:site_name" content="about.me/s.olaiya">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">        
    <link rel="shortcut icon" href="{{asset('public/img/favicons/favicon.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('public/img/favicons/favicon-192x192.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('public/img/favicons/apple-touch-icon-180x180.png')}}">
    <link rel="stylesheet" href="{{asset('public/js/plugins/fullcalendar/fullcalendar.min.css')}}">
    <link rel="stylesheet" id="css-main" href="{{asset('public/css/codebase.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/js/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/js/plugins/select2/select2-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/js/plugins/sweetalert2/sweetalert2.min.css')}}">
    <script src="{{asset('public/js/core/jquery.min.js')}}"></script>

    <style type="text/css">
    .btn{

        cursor : pointer !important;
    }

    .btn-xs{

        padding:1px 5px;
        font-size:12px;
        line-height:1.5;
        border-radius:3px;
        height:20px;
    }
</style>
</head>
<body>
 <div id="page-container" class="sidebar-mini sidebar-o side-scroll page-header-modern main-content-boxed">

     @include('layouts.includes.nav')

     <header id="page-header bg-light">
        <div class="content-header">
            <div class="content-header-section">
                <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                    <i class="fa fa-navicon"></i>
                </button>
            </div>

            <div class="content-header-section">

                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       {{Auth::user()->name}}<i class="fa fa-angle-down ml-5"></i>
                   </button>
                   <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                    <a class="dropdown-item" href="#">
                        <i class="si si-user mr-5"></i> Profile
                    </a>

                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="javascript:void(0)" onclick="$('#logout-form').submit()">
                        <i class="si si-logout mr-5"></i> Sign Out
                    </a>
                    <form method="post" style="display:none" action="{{url('logout')}}" id="logout-form">
                        {{csrf_field()}}
                    </form>
                </div>
            </div>
        </div>

    </div>


    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>

</header>

<main id="main-container">
    <div class="content">

        @yield('content')

    </div>

</main>

<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <div class="float-right">
            Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">Olaiya Segun</a>
        </div>
        <div class="float-left">
            <a class="font-w600" href="#" target="_blank">Stribe</a> &copy; <span class="js-year-copy">{{date('Y')}}</span>
        </div>
    </div>
</footer>

</div>

<script src="{{asset('public/js/core/popper.min.js')}}"></script>
<script src="{{asset('public/js/core/bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/core/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('public/js/core/jquery.scrollLock.min.js')}}"></script>
<script src="{{asset('public/js/core/jquery.appear.min.js')}}"></script>
<script src="{{asset('public/js/core/jquery.countTo.min.js')}}"></script>
<script src="{{asset('public/js/core/js.cookie.min.js')}}"></script>
<script src="{{asset('public/js/core/moment.min.js')}}"></script>
<script src="{{asset('public/js/codebase.js')}}"></script>
<script src="{{asset('public/js/plugins/fullcalendar/fullcalendar.min.js')}}"></script>
<script src="{{asset('public/js/plugins/chartjs/Chart.bundle.min.js')}}"></script>
<script src="{{asset('public/js/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('public/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('public/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
<script src="{{asset('public/js/plugins/sweetalert2/es6-promise.auto.min.js')}}"></script>
<script src="{{asset('public/js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('public/js/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script>
    $(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.select2').select2();
        $('.datepicker').datepicker({ format : 'dd-mm-yyyy' }).datepicker("setDate", new Date());

        $('.timepicker').timepicker();
        

        swal.setDefaults({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-lg btn-alt-success m-5',
            cancelButtonClass: 'btn btn-lg btn-alt-danger m-5',
            inputClass: 'form-control'
        });

        $('.message-success').click(function(){

            swal('Good job!', 'Message queued succesfully.', 'success' );
        })

        @if(session('status'))
        $.notify(
            { icon: 'fa fa-info-circle', message: '{{session('status')}}'},
            {type: 'info', allow_dismiss : false, delay: 5000}
        );
        @endif

        @if(session('error'))
        $.notify(
            { icon: 'fa fa-times', message: '{{session('error')}}'},
            {type: 'danger', allow_dismiss : false, delay: 5000}
        );
        @endif

    })

    function notify(content = '', type = 'info')
    {

        $.notify(
            { icon: 'fa fa-info-circle', message: content},
            {type: type, allow_dismiss : false, delay: 5000, z_index: 99999}
        );
    }
    
    </script>

    @yield('scripts')
    
    </body>
    </html>