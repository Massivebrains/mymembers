@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Home</a>
    <a class="breadcrumb-item" href="{{url('contacts')}}">contacts</a>
    <a class="breadcrumb-item" href="{{url('contact')}}">Upload</a>
    <span class="breadcrumb-item active">Guide</span>
</nav>


<div class="row gutters-tiny">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">contacts data upload Guide</h3>
            </div>
            <div class="block-content">
                
                <p>Your excel spreadsheet should follow the order below. <strong class="text-danger">Including the headings.</strong> <a href="{{asset('public/uploads/template.xlsx')}}" download>click here</a> to download a sample template.</p>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Firstname*</th>
                                <th>Surname*</th>
                                <th>Phone*</th>
                                <th>Email</th>
                                <th>Birth Month*</th>
                                <th>Birth Day*</th>
                                <th>Gender*</th>
                                <th>Language*</th>
                                <th>Age Bracket*</th>
                                <th>Groups*</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>John</td>
                                <td>Doe</td>
                                <td>08000000000</td>
                                <td>johndoe@gmail.com</td>
                                <td>1</td>
                                <td>1</td>
                                <td>male</td>
                                <td>English</td>
                                <td>youth</td>
                                <td>ushers, prayer warriors</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Language Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>English</td></tr>
                                    <tr><td>Yoruba</td></tr>
                                    <tr><td>Igbo</td></tr>
                                    <tr><td>Hausa</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Age Brackets Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($age_brackets as $row)
                                    <tr><td>{{ucfirst($row->name)}}</td></tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Groups Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($groups as $row)
                                    <tr><td>{{ucfirst($row->name)}}</td></tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-12" style="margin-bottom:20px;">
                         <a href="{{url('contact')}}" class="btn btn-primary pull-right">Back to Upload</a>
                    </div>
                </div>
                

            </div>
        </div>
    </div>
</div>
@endsection