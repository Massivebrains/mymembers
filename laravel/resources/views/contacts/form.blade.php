@extends('layouts.app')

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Home</a>
    <a class="breadcrumb-item" href="{{url('contacts')}}">contacts</a>
    <span class="breadcrumb-item active">New Contact</span>
</nav>


<div class="row gutters-tiny">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    {{$contact->firstname == '' ? 'New Contact' : $contact->firstname.' '.$contact->lastname}}
                </h3>
            </div>
            <div class="block-content">

                <div class="row">
                    <div class="col-sm-8 col-md-8">
                        <form action="{{url('contact', $contact->id)}}" method="POST" class="form-horizontal" role="form">

                            {{csrf_field()}}

                            <div class="form-group">
                                <fieldset class="form-group">
                                    <legend>Contact Details</legend>


                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-6">
                                            <label>Firstname *</label>
                                            <input type="text" name="firstname" class="form-control {{$errors->has('firstname') ? 'is-invalid' : ''}}" value="{{old('firstname', $contact->firstname)}}" required placeholder="e.g John">
                                            @if($errors->has('firstname'))
                                            <div class="invalid-feedback">{{$errors->first('firstname')}}</div>
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6">
                                            <label>Surname *</label>
                                            <input type="text" name="lastname" class="form-control {{$errors->has('lastname') ? 'is-invalid' : ''}}" value="{{old('lastname', $contact->lastname)}}" required placeholder="e.g Smith">
                                            @if($errors->has('lastname'))
                                            <div class="invalid-feedback">{{$errors->first('lastname')}}</div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-6">
                                            <label>Phone Number *</label>
                                            <input type="number" name="phone" class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}" value="{{old('phone', $contact->phone)}}" required placeholder="e.g 08175020329" maxlength="11" minlength="11">
                                            @if($errors->has('phone'))
                                            <div class="invalid-feedback">{{$errors->first('phone')}}</div>
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6">
                                            <label>Email</label>
                                            <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" value="{{old('email', $contact->email)}}" placeholder="e.g johnsmit@domain.com">
                                            @if($errors->has('email'))
                                            <div class="invalid-feedback">{{$errors->first('email')}}</div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-6">
                                            <label>Birth Month *</label>
                                            <select name="month" class="form-control select2">
                                                @for($i = 1; $i<=12; $i++)

                                                @if($contact->month == $i || old('month') == $i)
                                                <option value="{{$i}}" selected>{{config('settings.months')[$i]}}</option>
                                                @else
                                                <option value="{{$i}}">{{config('settings.months')[$i]}}</option>
                                                @endif

                                                @endfor
                                            </select>
                                        </div>

                                        <div class="form-group col-sm-6 col-md-6">
                                            <label>Birth Day</label>
                                            <input type="number" name="day" class="form-control {{$errors->has('day') ? 'is-invalid' : ''}}" value="{{old('day', $contact->day)}}" placeholder="e.g 1" min="1" max="31">
                                            @if($errors->has('day'))
                                            <div class="invalid-feedback">{{$errors->first('day')}}</div>
                                            @endif
                                        </div>



                                    </div>
                                    <hr>
                                    <div class="row">

                                        <div class="form-group col-sm-4 col-md-4">
                                            <label>Gender</label><br>

                                            @foreach(config('settings.gender') as $row)

                                            @if($contact->gender == $row || old('gender') == $row)
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" name="gender" value="{{$row}}" class="css-control-input" checked>
                                                <span class="css-control-indicator"></span> {{ucfirst($row)}}
                                            </label>
                                            @else
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" name="gender" value="{{$row}}" class="css-control-input">
                                                <span class="css-control-indicator"></span> {{ucfirst($row)}}
                                            </label>
                                            @endif

                                            @endforeach

                                        </div>

                                        <div class="form-group col-sm-4 col-md-4">
                                            <label>Marital Status</label><br>

                                            @foreach(config('settings.marital_status') as $row)

                                            @if($contact->marital_status == $row || old('marital_status') == $row)
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" name="marital_status" value="{{$row}}" class="css-control-input" checked>
                                                <span class="css-control-indicator"></span> {{ucfirst($row)}}
                                            </label>
                                            @else
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" name="marital_status" value="{{$row}}" class="css-control-input">
                                                <span class="css-control-indicator"></span> {{ucfirst($row)}}
                                            </label>
                                            @endif

                                            @endforeach

                                        </div>

                                        <div class="form-group col-sm-4 col-md-4">
                                            <label>State of Origin </label>
                                            <select class="form-control select2" name="state_id">

                                                @foreach(DB::table('states')->get() as $row)

                                                @if($contact->state_id == $row->state_id || old('state_id') == $row->state_id)
                                                <option value="{{$row->state_id}}" selected>{{ucfirst($row->name)}}</option>
                                                @else
                                                <option value="{{$row->state_id}}">{{ucfirst($row->name)}}</option>
                                                @endif

                                                @endforeach
                                            </select>
                                        </div>


                                    </div>
                                    <hr>

                                    <div class="row">



                                        <div class="form-group col-sm-6 col-md-6">
                                            <label>Preferred Language</label><br>

                                            @foreach(config('settings.languages') as $row)

                                            @if($contact->language == $row || old('language') == $row)
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" name="language" value="{{$row}}" class="css-control-input" checked>
                                                <span class="css-control-indicator"></span> {{strtoupper($row)}}
                                            </label>
                                            @else
                                            <label class="css-control css-control-primary css-radio">
                                                <input type="radio" name="language" value="{{$row}}" class="css-control-input">
                                                <span class="css-control-indicator"></span> {{strtoupper($row)}}
                                            </label>
                                            @endif

                                            @endforeach

                                        </div>


                                    </div>

                                    <hr>
                                    <div class="row">

                                        <div class="form-group col-sm-12 col-md-12">
                                            <label>Groups <a data-toggle="modal" href='#group-modal' class="badge badge-info"><i class="fa fa-plus-circle"></i> Add</a></label>
                                            <select class="form-control select2 group-select" name="groups[]" multiple>
                                                @foreach($groups as $row)

                                                @if(in_array($row->id, $contact_groups))
                                                <option value="{{$row->id}}" selected>
                                                    {{ucfirst($row->name)}}
                                                </option>
                                                @else
                                                <option value="{{$row->id}}">
                                                    {{ucfirst($row->name)}}
                                                </option>
                                                @endif

                                                @endforeach
                                            </select>
                                        </div>

                                    </div>


                                    <div class="row">


                                        <div class="form-group col-sm-12 col-md-12">
                                            <label>
                                                Age Bracket <a data-toggle="modal" href='#age-bracket-modal' class="badge badge-info"><i class="fa fa-plus-circle"></i> Add</a>
                                            </label>
                                            <br>

                                            <div class="age-brackets-radio">
                                                @foreach($age_brackets as $row)

                                                @if($contact->age_bracket_id == $row->id || old('age_bracket_id') == $row->id)
                                                <label class="css-control css-control-primary css-radio">
                                                    <input type="radio" name="age_bracket_id" value="{{$row->id}}" class="css-control-input" checked>
                                                    <span class="css-control-indicator"></span> {{strtoupper($row->name)}}
                                                </label>
                                                @else
                                                <label class="css-control css-control-primary css-radio">
                                                    <input type="radio" name="age_bracket_id" value="{{$row->id}}" class="css-control-input">
                                                    <span class="css-control-indicator"></span> {{strtoupper($row->name)}}
                                                </label>
                                                @endif

                                                @endforeach
                                            </div>

                                            @if($errors->has('age_bracket_id'))
                                            <div class="invalid-feedback">{{$errors->first('age_bracket_id')}}</div>
                                            @endif
                                        </div>

                                    </div>


                                    <button type="submit" class="btn btn-primary pull-right">
                                        <i class="si si-save"></i> Submit
                                    </button >
                                </fieldset>


                            </div>

                        </form>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <form method="post" action="{{url('contacts-upload')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <fieldset class="form-group">
                                <legend>Upload Bulk</legend>

                                <div class="alert alert-default"><a href="{{url('contacts-upload-guide')}}">Click here</a> to see guidelines on how to prepare your excel spreadsheet for upload.</div>

                                <div class="form-group row">
                                    <label class="col-12">Select File (.xlsx Only)</label>
                                    <div class="col-12">
                                        <input type="file" name="contacts" required>
                                    </div>

                                    @if($errors->has('contacts'))
                                    <div class="invalid-feedback">{{$errors->first('contacts')}}</div>
                                    @endif
                                </div>
                                <button type="submit" name="upload" class="btn btn-primary">
                                    Upload
                                </button>
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="age-bracket-modal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modal-slideright" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideright" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Age Bracket</h3>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                        <div class="input-group">
                            <input type="text" class="form-control age-bracket-name" placeholder="Enter New Age Bracket">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-secondary add-new-age-bracket-button">
                                    <i class="fa fa-plus"></i> Add New
                                </button>
                            </span>
                        </div>

                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th style="width: 60%">Age Bracket</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="age-bracket-table-body">
                                @foreach($age_brackets as $row)
                                <tr>
                                    <td>{{strtoupper($row->name)}}</td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm delete" data-id="{{$row->id}}" data-tapped="0">
                                            <i class="fa fa-times"></i> Delete
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary dismiss-age-bracket-modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="group-modal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modal-slideright" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideright" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">contact Groups</h3>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                        <div class="input-group">
                            <input type="text" class="form-control group-name" placeholder="Enter New Group Name">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-secondary add-new-group-button">
                                    <i class="fa fa-plus"></i> Add New
                                </button>
                            </span>
                        </div>

                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th style="width: 60%">Group</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="group-table-body">
                                @foreach($groups as $row)
                                <tr>
                                    <td>{{strtoupper($row->name)}}</td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm delete-group" data-id="{{$row->id}}" data-tapped="0">
                                            <i class="fa fa-times"></i> Delete
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary dismiss-group-modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $('body').delegate('.delete', 'click', function(){

        id           = $(this).data('id');
        $element     = $('.delete[data-id="'+id+'"]');

        if($element.data('tapped') == '1'){

            $.get('ajax/destory-age-bracket/'+id, function(response){

                $element.parents('tr').remove();
            });

        }else{

            $element.html('<i class="fa fa-spin fa-spinner"></i> Retap to Confirm').data('tapped', '1');

            setTimeout(function(){

                $element.html('<i class="fa fa-times"></i> Delete').data('tapped', '0');

            }, 2000);
        }


    })


    $('.add-new-age-bracket-button').click(function(){

        $.post('{{url('ajax/age-brackets')}}', {'name' : $('.age-bracket-name').val()}, function(response){

            $('.age-bracket-table-body').empty();

            $.each(response, function(index, value){

                $('.age-bracket-table-body').append('<tr><td>'+value.name.toUpperCase()+'</td><td><button type="button" class="btn btn-danger btn-sm delete" data-id="'+value.id+'" data-tapped="0"><i class="fa fa-times"></i> Delete</button></td></tr>');
            });

        }).done(function(){

            $('.age-bracket-name').val('');
            $('.add-new-age-bracket-button').prop('disabled', false);
            notify('Age brackets updated succesfully');
        })

    });

    $('.dismiss-age-bracket-modal').click(function(){

        $.post('{{url('ajax/age-brackets')}}', {}, function(response){

            $('.age-brackets-radio').empty();

            $.each(response, function(index, value){

                $('.age-brackets-radio').append('<label class="css-control css-control-primary css-radio"><input type="radio" name="age_bracket_id" value="'+value.id+'" class="css-control-input"><span class="css-control-indicator"></span> '+value.name.toUpperCase()+'</label>');
            })
        })
    })



    $('body').delegate('.delete-group', 'click', function(){

        id           = $(this).data('id');
        $element     = $('.delete-group[data-id="'+id+'"]');

        if($element.data('tapped') == '1'){

            $.get('ajax/destory-group/'+id, function(response){

                $element.parents('tr').remove();
            });

        }else{

            $element.html('<i class="fa fa-spin fa-spinner"></i> Retap to Confirm').data('tapped', '1');

            setTimeout(function(){

                $element.html('<i class="fa fa-times"></i> Delete').data('tapped', '0');

            }, 2000);
        }


    })


    $('.add-new-group-button').click(function(){

        $.post('{{url('ajax/groups')}}', {'name' : $('.group-name').val()}, function(response){

            $('.group-table-body').empty();

            $.each(response, function(index, value){

                $('.group-table-body').append('<tr><td>'+value.name.toUpperCase()+'</td><td><button type="button" class="btn btn-danger btn-sm delete-group" data-id="'+value.id+'" data-tapped="0"><i class="fa fa-times"></i> Delete</button></td></tr>');
            });

        }).done(function(){

            $('.group-name').val('');
            $('.add-new-group-button').prop('disabled', false);
            notify('Groups updated succesfully');
        })

    });

    $('.dismiss-group-modal').click(function(){

        $.post('{{url('ajax/groups')}}', {}, function(response){

            $('.group-select').empty();

            $.each(response, function(index, value){

                $('.group-select').append('<option value="'+value.id+'">'+value.name.toUpperCase()+'</option>');
            })
        })
    })

</script>
@endsection