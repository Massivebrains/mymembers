@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Home</a>
    <span class="breadcrumb-item active">Contacts</span>
</nav>


<div class="row gutters-tiny">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Contacts</h3>
                <a href="{{url('contact')}}" class="btn btn-primary btn-sm pull-right">
                    <i class="fa fa-plus"></i> Create New Contact(s)
                </a>
            </div>
            <div class="block-content">
                <form action="{{url('contacts-search')}}" method="POST">
                    {{csrf_field()}}
                    <div class="input-group">
                        <input type="text" name="query" required class="form-control" placeholder="Search by firstname, lastname, email, phone number and more..">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-secondary">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </span>
                    </div>

                    @if(isset($query))
                    <div class="font-size-h3 font-w600 py-30 mb-20 text-center border-b">
                        <span class="text-primary font-w700">{{count($contacts)}}</span> {{str_plural('contact', count($contacts))}} found for <mark class="text-danger">{{$query}}</mark>
                    </div>
                    @endif
                </form>
                <div class="table-responsive">
                    <table class="table table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Group(s)</th>
                                <th>Birthdate</th>
                                <th>Language</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($contacts as $row)
                            
                            <tr>
                                <td>{{_title($row->gender, isset($row->age_bracket->name) ? $row->age_bracket->name : 0).' '.ucfirst($row->firstname.' '.$row->lastname)}}</td>
                                <td>{{$row->phone}}</td>
                                <td>{{$row->email}}</td>
                                <td>
                                    @foreach($row->groups as $group)
                                    <label class="badge badge-primary">{{ucfirst($group->name)}}</label>
                                    @endforeach
                                </td>
                                <td>{{config('settings.months')[$row->month].' '.$row->day}}</td>
                                <td>{{ucfirst($row->language)}}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{url('contact', $row->id)}}" class="btn btn-sm btn-secondary"  data-toggle="tooltip" title="View contact Information">
                                            <i class="fa fa-eye"></i> View
                                        </a>
                                    </div>

                                </td>
                            </tr>
                            @endforeach

                            @if(count($contacts) < 1 && !isset($query))
                            <tr>
                                <td colspan="7" class="text-center">when you create or upload your contacts, they would appear here.</td>
                            </tr>
                            @endif
                            
                            @if(!isset($query))
                            <tr>
                                <td colspan="7">
                                    {{$contacts->links('vendor.pagination.index')}}
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection