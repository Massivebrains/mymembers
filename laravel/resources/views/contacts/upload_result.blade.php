@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Home</a>
    <a class="breadcrumb-item" href="{{url('contacts')}}">contacts</a>
    <a class="breadcrumb-item" href="{{url('contact')}}">Upload</a>
    <span class="breadcrumb-item active">Result</span>
</nav>


<div class="row gutters-tiny">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Upload Result</h3>
            </div>
            <div class="block-content">

                <div class="col-sm-4 col-md-4">

                    <ul class="list-group push">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                           Total Uploads
                           <span class="badge badge-pill badge-primary">
                            {{$error_count + $success_count}}
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Contacts Succesfully Saved
                        <span class="badge badge-pill badge-success">
                            {{$success_count}}
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Errors
                        <span class="badge badge-pill badge-danger">
                            {{$error_count}}
                        </span>
                    </li>

                </ul>

            </div>
            <br>
            @if(count($errors) > 0)
            <div class="table-responsive">
                <h3 class="block-title">Error Details</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Firstname</th>
                            <th>Surname</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Birth Month</th>
                            <th>Birth Day</th>
                            <th>Gender</th>
                            <th>Language</th>
                            <th>Age Bracket</th>
                            <th>Groups</th>

                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 0 @endphp

                        @foreach($error_rows as $row)
                        <tr>
                            <td>{{$row->firstname}}</td>
                            <td>{{$row->surname}}</td>
                            <td>{{$row->phone}}</td>
                            <td>{{$row->email}}</td>
                            <td>{{$row->birth_month}}</td>
                            <td>{{$row->birth_day}}</td>
                            <td>{{$row->gender}}</td>
                            <td>{{$row->language}}</td>
                            <td>{{$row->age_bracket}}</td>
                            <td>{{$row->groups}}</td>
                        </tr>
                        <tr>
                            <td colspan="10">
                                <span class="text-danger">{!! @$errors[$i] !!}</span>
                            </td>
                        </tr>

                        @php $i++ @endphp

                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif



            <div class="row">
                <div class="col-12" style="margin-bottom:20px;">
                 <a href="{{url('contacts')}}" class="btn btn-primary pull-right">Back to contacts</a>
             </div>
         </div>


     </div>
 </div>
</div>
</div>
@endsection