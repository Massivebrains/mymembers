@extends('layouts.app')

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Home</a>
    <span class="breadcrumb-item active">Buy SMS Credites</span>
</nav>


<div class="row gutters-tiny invisible" data-toggle="appear">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Buy SMS Credits</h3>
                <p>SMS credits costs N3.20 per Unit</p>
            </div>
            <div class="block-content">

                <div class="row">

                    <div class="col-sm-12 col-md-12">
                        <div class="row">
                            <div class="col-sm-3 col-md-3"></div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-secondary">
                                                <i class="fa fa-calculator"></i> Enter Quantity
                                            </button>
                                        </span>
                                        <input type="text" class="form-control" placeholder="100" id="input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <div class="block-content block-content-full">
                                <div class="row py-20">
                                    <div class="col-6 text-right border-r">
                                        <div class="font-size-h3 font-w600" id="units">0</div>
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Units</div>
                                    </div>
                                    <div class="col-6">
                                        <div class="font-size-h3 font-w600" id="total">0</div>
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Total</div>
                                    </div>
                                </div>

                                <div class="row py-20">
                                    <div class="col-4"></div>
                                    <div class="col-4 text-center">
                                        <input type="text" name="" id="input" class="form-control" placeholder="Have a coupon? Enter it here"><br>
                                        <button type="submit" class="btn btn-primary text-center">Continue To Payment</button><br>
                                        <img src="{{asset('public/img/avatars/paywith.png')}}" alt="" class="img">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')

<script type="text/javascript">
    
    $(() => {

        $('#input').on('keyup', () => {

            let units = $('#input').val();

            $('#units').html(units);
            $('#total').html(units*3.2);
        })
    });
</script>
@endsection