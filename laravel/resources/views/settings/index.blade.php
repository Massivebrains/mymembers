@extends('layouts.app')

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Home</a>
    <span class="breadcrumb-item active">Settings</span>
</nav>


<div class="row gutters-tiny">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Settings</h3>
            </div>
            <div class="block-content">

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <form action="{{url('/settings/save')}}" method="POST" class="form-horizontal" role="form">
                            {{csrf_field()}}

                            <fieldset class="form-group">
                                <legend>General Settings</legend>


                                <div class="row">
                                    <div class="form-group col-sm-12 col-md-12">
                                        <label>Sender's Name *</label>
                                        <input type="text" name="sender_name" class="form-control {{$errors->has('sender_name') ? 'is-invalid' : ''}}" value="{{old('sender_name', $setting->sender_name)}}" required placeholder="e.g {{Auth::user()->name}}">
                                        @if($errors->has('sender_name'))
                                        <div class="invalid-feedback">{{$errors->first('sender_name')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                   <div class="form-group col-sm-12 col-md-12">
                                    <label>Address contacts as</label><br>

                                    @foreach(['mr_mrs_miss', 'bro_sis', 'dear'] as $row)

                                    @if($row == $setting->contact_address || $row == old('contact_address'))
                                    <label class="css-control css-control-primary css-radio">
                                        <input type="radio" name="contact_address" value="{{$row}}" class="css-control-input" checked required>
                                        <span class="css-control-indicator"></span> 
                                        {{strtoupper(implode('/', explode('_', $row)))}}
                                    </label>
                                    @else
                                    <label class="css-control css-control-primary css-radio">
                                        <input type="radio" name="contact_address" value="{{$row}}" class="css-control-input" required>
                                        <span class="css-control-indicator"></span> 
                                        {{strtoupper(implode('/', explode('_', $row)))}}
                                    </label>
                                    @endif
                                    
                                    @endforeach
                                </div>

                            </div>
                        </fieldset>

                        <fieldset class="form-group">
                           <legend>Default Birthday Messsage Settings</legend>

                           <div class="form-group">
                            <div class="alert alert-info">
                                Use <strong>#name#</strong> to personalize message. <i>#name# would be replaced by the actual name of the contact.</i>
                            </div>
                            <label for="">Message</label>
                            <textarea name="" class="form-control" rows="3"></textarea>
                        </div>

                        <div class="row">
                           <div class="form-group col-sm-6 col-md-6">
                            <label>Time to send Birthday Messages</label>
                            <input type="text" name="birthday_sms_time" class="form-control timepicker {{$errors->has('birthday_sms_time') ? 'is-invalid' : ''}}" value="{{old('birthday_sms_time', $setting->birthday_sms_time)}}" required>
                            @if($errors->has('birthday_sms_time'))
                            <div class="invalid-feedback">{{$errors->first('sender_name')}}</div>
                            @endif
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label data-toggle="tooltip" title="A birthday reminder message would be sent to this number a day before any birthday in your list"><i class="fa fa-information-circle"></i> Send Birthday Reminder To</label>
                            <input type="text" name="birthday_reminder_number" class="form-control {{$errors->has('birthday_reminder_number') ? 'is-invalid' : ''}}" value="{{old('birthday_reminder_number', $setting->birthday_reminder_number)}}" required placeholder="08000000000">
                            @if($errors->has('birthday_reminder_number'))
                            <div class="invalid-feedback">{{$errors->first('birthday_reminder_number')}}</div>
                            @endif
                        </div>
                    </div>  

                    <button type="submit" class="btn btn-primary pull-right">Save All</button>
                </fieldset>

            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection