@extends('layouts.auth')

@section('content')
<div class="row" style="margin-top: 10%">
    <div class="col-sm-4 col-md-4"></div>
    <div class="col-sm-4 col-md-4">

        <div class="block block-themed">
            <div class="block-header bg-primary">
                <img src="{{asset('public/landing')}}/images/logo-white.png">
            </div>
            <div class="block-content">
                <form action="#" method="post" action="{{route('password.email')}}">
                    {{csrf_field()}}

                    @if(session('status'))
                    <div class="alert alert-success">
                        {!! session('status') !!}
                    </div>
                    @endif

                    <div class="form-group row">
                        <label class="col-12" for="login1-username">Email</label>
                        <div class="col-12">
                            <input type="text" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" name="email" placeholder="Enter your email.." value="{{old('email')}}">
                            @if($errors->has('email'))
                            <div class="invalid-feedback">{{$errors->first('email')}}</div>
                            @endif
                        </div>
                    </div>
                    
                    
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-alt-primary">
                                <i class="fa fa-arrow-right mr-5"></i> Send Password Reset Link
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div class="col-sm-4 col-md-4"></div>
</div>

@endsection