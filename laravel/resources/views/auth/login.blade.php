@extends('layouts.auth')

@section('content')
<div class="row" style="margin-top: 10%">
    <div class="col-sm-4 col-md-4"></div>
    <div class="col-sm-4 col-md-4">

        <div class="block block-themed">
            <div class="block-header bg-primary">
                <h3 class="block-title">
                    <img src="{{asset('public/landing')}}/images/logo-white.png">
                </h3>
                
            </div>
            <div class="block-content">
                <form action="#" method="post" action="{{url('login')}}">
                    {{csrf_field()}}
                    @if (session('status'))
                        <div class="alert alert-success">
                            {!! session('status') !!}
                        </div>
                    @endif
                    <div class="form-group row">
                        <label class="col-12" for="login1-username">Email</label>
                        <div class="col-12">
                            <input type="text" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" name="email" placeholder="Enter your email.." value="{{old('email')}}" required autofocus>
                            @if($errors->has('email'))
                            <div class="invalid-feedback">{{$errors->first('email')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="login1-password">Password</label>
                        <div class="col-12">
                            <input type="password" class="form-control" {{$errors->has('password') ? 'is-invalid' : ''}} name="password" placeholder="Enter your password.." required>
                            @if($errors->has('password'))
                            <div class="invalid-feedback">{{$errors->first('password')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="css-control css-control-sm css-control-primary css-switch">
                                <input type="checkbox" class="css-control-input"  name="remember" {{old('remember') ? 'checked' : ''}}>
                                <span class="css-control-indicator"></span> Remember Me?
                            </label>
                            <a href="#" class="pull-right">Forgot Your Password?</a>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                          <button type="submit" class="btn btn-alt-primary">
                            <i class="fa fa-arrow-right mr-5"></i> Log in
                        </button>
                        <a href="{{url('register')}}" class="pull-right">Dont have an account?</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
<div class="col-sm-4 col-md-4"></div>
</div>
@endsection