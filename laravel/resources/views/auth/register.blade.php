@extends('layouts.auth')

@section('content')
<div class="row" style="margin-top: 25px">
  <div class="col-sm-3 col-md-3"></div>
  <div class="col-sm-6 col-md-6">

    <div class="block block-themed">
      <div class="block-header bg-primary">
        <img src="{{asset('public/landing')}}/images/logo-white.png">
      </div>
      <div class="block-content">
        <form method="post" action="{{url('register')}}">
          {{csrf_field()}}
          @if (session('status'))
          <div class="alert alert-success">
            {!! session('status') !!}
          </div>
          @endif
          <div class="form-group">
           <label>Organisation Name</label>
           <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" placeholder="Organisation Name or Short Description" value="{{old('name')}}" requied autofocus>
           @if($errors->has('name'))
           <div class="invalid-feedback">{{$errors->first('name')}}</div>
           @endif
         </div> 

         <div class="form-group">
           <label>Organisation Address</label>
           <input type="text" class="form-control {{$errors->has('address') ? 'is-invalid' : ''}}" name="address" placeholder="Full Verifyiable address" value="{{old('address')}}" required>
           @if($errors->has('address'))
           <div class="invalid-feedback">{{$errors->first('address')}}</div>
           @endif
         </div>

         <div class="form-group">
          <label>Contact Person's Email</label>
          <div class="input-group">
            <input type="email" class="form-control  {{$errors->has('email') ? 'is-invalid' : ''}}" name="email" placeholder="Enter your email.." value="{{old('email')}}">
            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
          </div>
          @if($errors->has('email'))
          <div class="invalid-feedback">{{$errors->first('email')}}</div>
          @endif
        </div>

        <div class="form-group">
          <label>Contact Person's Phone</label>
          <div class="input-group">
            <input type="text" class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}" name="phone" placeholder="08000000000" value="{{old('phone')}}">
            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
          </div>
          @if($errors->has('phone'))
          <div class="invalid-feedback">{{$errors->first('phone')}}</div>
          @endif
        </div>

        <div class="form-group row">
          <label class="col-12">Password</label>
          <div class="col-12">
            <div class="input-group">
              <input type="password" class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}" name="password" placeholder="Enter your password.." required>
              <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
            </div>
            @if($errors->has('password'))
            <div class="invalid-feedback">{{$errors->first('password')}}</div>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label class="col-12" for="register4-password2">Confirm Password</label>
          <div class="col-12">
            <div class="input-group">
              <input type="password" class="form-control" name="password_confirmation" placeholder="Retype your password.." required>
              <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
            </div>
            @if($errors->has('password_confirmation'))
            <div class="invalid-feedback">{{$errors->first('password_confirmation')}}</div>
            @endif
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12">
            <label class="css-control css-control-primary css-checkbox">
              <input type="checkbox" class="css-control-input" id="register4-terms" name="register4-terms">
              <span class="css-control-indicator"></span> Agree to terms?
            </label>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12">
            <button type="submit" class="btn btn-alt-primary">
              <i class="fa fa-plus mr-5"></i> Register
            </button>
            <a href="{{url('login')}}" class="pull-right">Already have an account?</a>
          </div>
        </div>
      </form>
    </div>
  </div>

</div>
<div class="col-sm-3 col-md-3"></div>
</div>

@endsection