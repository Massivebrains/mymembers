@extends('layouts.app')

@section('content')


<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="javascript:void(0)">Home</a>
    <span class="breadcrumb-item active">Reports</span>
</nav>


<div class="row gutters-tiny invisible" data-toggle="appear">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Reports</h3>

            </div>
            <div class="block-content">
                <div class="row gutters-tiny">
                    <div class="col-3 col-md-3 col-xl-3">
                        <a class="block block-transparent text-center bg-primary" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="font-size-h1 text-white">
                                    <strong>0K</strong>
                                </p>
                                <p class="font-w600 text-white-op">Members</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 col-md-3 col-xl-3">
                        <a class="block block-transparent text-center bg-success" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="font-size-h1 text-white">
                                    <strong>0K</strong>
                                </p>
                                <p class="font-w600 text-white-op">
                                    <i class="fa fa-envelope mr-5"></i> Messages Sent
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 col-md-3 col-xl-3">
                        <a class="block block-transparent text-center bg-gd-sun" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="font-size-h1 text-white">
                                    <strong>0</strong>
                                </p>
                                <p class="font-w600 text-white-op">
                                    <i class="fa fa-shopping-cart mr-5"></i> Birthday SMS Sent
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 col-md-3 col-xl-3">
                        <a class="block block-transparent text-center bg-corporate" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="font-size-h1 text-white">
                                    <strong>00.00</strong>
                                </p>
                                <p class="font-w600 text-white-op">
                                    <i class="fa fa-line-chart mr-5"></i> SMS Units Bal.
                                </p>
                            </div>
                        </a>
                    </div>
                    
                </div>
                {{-- <hr>
                <div class="row gutters-tiny">
                    <div class="table-responsive">
                        <h3 class="font-w300">Messages</h3>
                        <table class="table table-striped table-vcenter">
                            <thead>
                                <tr>
                                    <th>Message</th>
                                    <th>Reciepients</th>
                                    <th>SMS Units Used</th>
                                    <th>Date Sent</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td style="width:60%">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostr....</td>
                                    <td>458</td>
                                    <td>543</td>
                                    <td>Nov 5, 2017</td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                </div> --}}
                
            </div>
        </div>
    </div>
</div>

@endsection