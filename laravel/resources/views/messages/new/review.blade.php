@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
	<a class="breadcrumb-item" href="{{url('home')}}">Home</a>
	<a class="breadcrumb-item" href="{{url('message/new')}}">New Message</a>
	<a class="breadcrumb-item" href="{{url('message/new/'.$message_id.'/'.md5(time()))}}">Message Body</a>
	<a class="breadcrumb-item" href="{{url('message/recipients/'.$message_id.'/'.md5(time()))}}">Recipients</a>
	<a class="breadcrumb-item" href="{{url('message/options/'.$message_id.'/'.md5(time()))}}">Other Options</a>
	<span class="breadcrumb-item active">Review</span>
</nav>

<div class="row gutters-tiny invisible" data-toggle="appear">
	<div class="col-12">
		<div class="block">
			<div class="block-header block-header-default">
				<h3 class="block-title">Review Message</h3>
			</div>
			<div class="block-content">
				<div class="row">
					<div class="col-sm-12 col-md-12 review">
						<div class="table-responsive">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th style="width:25%">Message Body</th>
										<td colspan="2">{{$message->body}}</td>
									</tr>
									<tr>
										<th>Language Translation</th>
										<td colspan="2">
											@if($message->translate == 1)
											Allowed <em class="text-info pull-right"><i class="fa fa-info-circle"></i> Message would be translated to contacts's perferred languages.</em>
											@else
											Not Allowed
											@endif
										</td>
									</tr>
									<tr>
										<th>Recipients</th>
										<td colspan="2">
											
											{{count($recipients)}} contact(s) 

											<a data-toggle="modal" href='#contacts-modal' class="badge badge-info"><i class="fa fa-eye"></i> Show contacts</a>
										</td>
									</tr> 
									<tr>
										<th>Scheduled</th>
										<td colspan="2">
											{{_date($message->schedule_date, true)}} at {{_time($message->schedule_time)}}
										</td>
									</tr>
									<tr>
										<th>Repetition</th>
										<td colspan="2">

											@if($message->repitition_type == 'none')
											One time Message
											
											@elseif($message->repitition_type == 'daily')

											{{ucfirst($message->repitition_type)}}, every {{$message->repitition_value}} day(s) <em class="text-danger">at {{_time($message->schedule_time)}} starting {{_date($message->schedule_date, true)}}.</em>

											@elseif($message->repitition_type == 'weekly')

											{{ucfirst($message->repitition_type)}}, every {{$message->repitition_value}} week(s) <em class="text-danger">at {{_time($message->schedule_time)}} starting {{_date($message->schedule_date, true)}}.</em>

											@else

											{{ucfirst($message->repitition_type)}} every {{_date($message->schedule_date, true)}} at {{_time($message->schedule_time)}}

											@endif
										</td>
									</tr>
									
									<tr>
										<td colspan="3">
											<button type="submit" class="btn btn-success pull-right message-success" id="submit">
												<i class="fa fa-check"></i> Submit
											</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="contacts-modal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modal-slideright" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-slideright" role="document">
		<div class="modal-content">
			<div class="block block-themed block-transparent mb-0">
				<div class="block-header bg-primary-dark">
					<h3 class="block-title">Message Recipients</h3>
				</div>
				<div class="block-content" style="max-height:300px; overflow: scroll;">

					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>Name</th>
								<th>Phone Number</th>
							</tr>
							@foreach($recipients as $row)
								<tr>
									<th>{{ucfirst($row->firstname.' '.$row->lastname)}}</th>
									<td>{{$row->phone}}</td>
								</tr>
							@endforeach

						</tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-alt-secondary dismiss-group-modal" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
	$(() => {

		$('#submit').click(() => {

			$.get('{{url('message/queue/'.$message->id)}}', (response) => {

				if(response == 1){

					setTimeout(() => {

						window.location = '{{url('messages')}}';

					}, 2000)
				}
			})
		})
	})
</script>

@endsection