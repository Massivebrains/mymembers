@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Home</a>
    <a class="breadcrumb-item" href="{{url('message/new')}}">New Message</a>
    <a class="breadcrumb-item" href="{{url('message/new/'.$message_id.'/'.md5(time()))}}">Message Body</a>
    <span class="breadcrumb-item active">Message recipients</span>
</nav>

<form action="{{url('message/save-recipients')}}" method="post">

    {{ csrf_field() }}

    <div class="row gutters-tiny invisible" data-toggle="appear">
        <div class="col-12">
            <div class="block">

                <div class="block-header block-header-default">
                    <h3 class="block-title">New Message</h3>
                </div>

                <div class="block-content">

                    <div class="form-group">

                        <fieldset class="form-group">
                            <legend>Recipients</legend>

                            <div class="row">

                                <div class="col-sm-3 col-md-3">
                                    <label class="css-control css-control-primary css-radio">
                                        <input type="radio" value="all" class="css-control-input" name="recipients" checked>
                                        <span class="css-control-indicator"></span> 
                                        <span data-toggle="tooltip" title="All the active contacts on your list gets this message.">
                                        Send message to all Contacts</span>
                                    </label>
                                </div>

                                <div class="col-sm-3 col-md-3">
                                    <label class="css-control css-control-primary css-radio">
                                        <input type="radio" value="customize" class="css-control-input" name="recipients">
                                        <span class="css-control-indicator"></span> 
                                        <span data-toggle="tooltip" title="Choose people who recieves this message">
                                        Customize Recipients</span>
                                    </label> 
                                </div>

                                <div class="col-sm-3 col-md-3">
                                    <label class="css-control css-control-primary css-radio">
                                        <input type="radio" value="custom" class="css-control-input" name="recipients">
                                        <span class="css-control-indicator"></span> 
                                        <span data-toggle="tooltip" title="Enter recipients phone number">
                                        Enter Recipients</span>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div id="customize" style="display:none">

                                        <div class="table-responsive">
                                            <table class="table table-striped table-vcenter">
                                                <tbody>
                                                    <tr>
                                                        <th style="width:30%">Age Bracket</th>
                                                        <td>
                                                            @foreach($age_brackets as $row)
                                                            <label class="css-control css-control-primary css-checkbox">
                                                                <input type="checkbox" name="age_bracket[]" class="css-control-input" value="{{$row->id}}">
                                                                <span class="css-control-indicator"></span> 
                                                                <span>{{ucfirst($row->name)}}</span>
                                                            </label>
                                                            @endforeach
                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <th>Gender</th>
                                                        <td>
                                                            <label class="css-control css-control-primary css-checkbox">
                                                                <input type="checkbox" name="gender[]" class="css-control-input" value="male">
                                                                <span class="css-control-indicator"></span> 
                                                                <span>Male</span>
                                                            </label>
                                                            <label class="css-control css-control-primary css-checkbox">
                                                                <input type="checkbox" name="gender[]" class="css-control-input" value="famale">
                                                                <span class="css-control-indicator"></span> 
                                                                <span>Female</span>
                                                            </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>Marial Status</th>
                                                        <td>
                                                            <label class="css-control css-control-primary css-checkbox">
                                                                <input type="checkbox" name="marital_status[]" class="css-control-input" value="single" checked>
                                                                <span class="css-control-indicator"></span> 
                                                                <span>Singles</span>
                                                            </label>
                                                            <label class="css-control css-control-primary css-checkbox">
                                                                <input type="checkbox" name="marital_status[]" class="css-control-input" value="married">
                                                                <span class="css-control-indicator"></span> 
                                                                <span>Married</span>
                                                            </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>Birth Month</th>
                                                        <td id="month">
                                                            <div class="all-months">
                                                                Send to all. <a href="javascript:void(0);" class="change-month">Change</a>
                                                            </div>
                                                            <div class="select-month" style="display:none;">
                                                                <div class="form-group">
                                                                    <select name="months[]" id="months" class="form-control select2" multiple style="width:100%">

                                                                        @foreach(_months() as $index => $value)
                                                                        <option value="{{$index}}">
                                                                            {{$value}} Born
                                                                        </option>
                                                                        @endforeach
                                                                    </select>
                                                                    <a href="javascript:void(0);" class="change-month">Select All</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>State of Origin</th>
                                                        <td id="state">
                                                            <div class="all-states">
                                                                Send to all. 
                                                                <a href="javascript:void(0);" class="change-state">Change</a>
                                                            </div>
                                                            <div class="select-state" style="display:none;">
                                                                <div class="form-group">
                                                                    <select name="states[]" id="states" class="form-control select2" multiple style="width:100%">
                                                                        @foreach($states as $row)
                                                                        <option value="{{$row->state_id}}">
                                                                            {{$row->name}}
                                                                        </option>
                                                                        @endforeach
                                                                    </select>
                                                                    <a href="javascript:void(0);" class="change-state">Select All</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>Group</th>
                                                        <td id="contact-group">
                                                            <div class="all-contact-group">
                                                                Send to all. 
                                                                <a href="javascript:void(0);" class="change-contact-group">Change</a>
                                                            </div>
                                                            <div class="select-contact-group" style="display:none;">
                                                                <div class="form-group">
                                                                    <select name="groups[]" id="groups" class="form-control select2" multiple style="width:100%">
                                                                        @foreach($groups as $row)
                                                                        <option value="{{$row->id}}">
                                                                            {{ucfirst($row->name)}}
                                                                        </option>
                                                                        @endforeach
                                                                    </select>
                                                                    <a href="javascript:void(0);" class="change-contact-group">Select All</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>


                                    <div id="custom" style="display: none">
                                        <div class="form-group">
                                            <label>recipients <small>Enter phone numbers separated by comma(,)</small></label>
                                            <textarea name="custom_recipients" class="form-control" rows="3" placeholder="080000000, 080000000"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top:15px;">
                                <div class="col-12">
                                    <input type="hidden" name="message_id" value="{{$message_id}}">
                                    <button type="submit" class="btn btn-primary pull-right">
                                        Next <i class="si si-arrow-right"></i> 
                                    </button>
                                </div>
                            </div>
                        </fieldset>

                    </div>


                </div>
            </div>
        </div>
    </div>
</form>


@endsection


@section('scripts')

<script type="text/javascript">

    $(function(){

        $('input[name="recipients"]').change(function(){

            let type = $('input[name="recipients"]:checked').val();

            if(type == 'customize'){

                $('#customize').show();
                $('#custom').hide();

            }else if(type == 'custom'){

                $('#customize').hide();
                $('#custom').show();

            }else{
            	
                $('#customize').hide();
                $('#custom').hide();
            }


        });

        $('.change-state').click(function(){

            $('#states').val('').change();
            $('.all-states').toggle();
            $('.select-state').toggle();
        })

        $('.change-month').click(function(){

            $('#months').val('').change();
            $('.all-months').toggle();
            $('.select-month').toggle();
        })

        $('.change-contact-group').click(function(){

            $('#groups').val('').change();
            $('.all-contact-group').toggle();
            $('.select-contact-group').toggle();
        })
    })
</script>


@endsection