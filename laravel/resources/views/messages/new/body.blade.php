@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Home</a>
    <a class="breadcrumb-item" href="{{url('message/new')}}">New Message</a>
    <span class="breadcrumb-item active">Message Details</span>
</nav>


<div class="row gutters-tiny invisible" data-toggle="appear">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">New Message</h3>
            </div>
            <div class="block-content">

                <form action="{{url('/message/new/'.$message->id)}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <fieldset class="form-group">
                            <legend>Message Details</legend>
                            <div class="form-group">
                                <div class="alert alert-info">
                                    Use <strong>#name#</strong> to personalize message. <i>#name# would be replaced by the actual name of the contact.</i>
                                </div>
                                <label>Message <small> <i class="fa fa-language"></i><i> English</i></small></label>
                                <textarea name="body" class="form-control {{$errors->has('body') ? 'is-invalid' : ''}}"" rows="5" required>{{old('body', $message->body)}}</textarea>
                                @if($errors->has('body'))
                                <div class="invalid-feedback">{{$errors->first('body')}}</div>
                                @endif
                            </div>
                            <label class="css-control css-control-primary css-checkbox">
                                @php
                                $checked = old('body', $message->translate) == true ? 'checked' : '';
                                @endphp
                                <input type="checkbox" class="css-control-input" name="translate" {{$checked}}>
                                <span class="css-control-indicator"></span> 
                                <span data-toggle="tooltip" title="Let system send sms in different languages as defined by the default languages of contacts in your list.">
                                    Automatically translate to contact's default language. <a href="#">Learn more...</a></span>
                                </label>

                                <div class="row" style="margin-top:20px;">
                                    <div class="col-sm-12 col-md-12">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            Next <i class="si si-arrow-right"></i> 
                                        </button>
                                    </div>
                                </div>
                            </fieldset>



                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    @endsection