@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
	<a class="breadcrumb-item" href="{{url('home')}}">Home</a>
	<a class="breadcrumb-item" href="{{url('message/new')}}">New Message</a>
	<a class="breadcrumb-item" href="{{url('message/new/'.$message_id.'/'.md5(time()))}}">Message Body</a>
	<a class="breadcrumb-item" href="{{url('message/recipients/'.$message_id.'/'.md5(time()))}}">Recipients</a>
	<span class="breadcrumb-item active">Other Options</span>
</nav>

<form action="{{url('/message/save-options')}}" method="post">
	{{csrf_field()}}
	<div class="row gutters-tiny invisible" data-toggle="appear">
		<div class="col-12">
			<div class="block">
				<div class="block-header block-header-default">
					<h3 class="block-title">New Message</h3>
				</div>
				<div class="block-content">
					<div class="form-group">

						<fieldset class="form-group">
							<legend>Other Options</legend>
							<div class="table-responsive">
								<table class="table table-striped">
									<tbody>
										<tr>
											<th>Schedule when to send this message</th>
											<td>
												<div class="row">
													<div class="col-sm-6 col-md-6">
														<div class="form-group">
															<input type="text" name="schedule_date" class="form-control datepicker" data-date-format='dd-mm-yyyy'>
														</div>
													</div>
													<div class="col-sm-6 col-md-6">
														<div class="form-group">
															<input type="text" name="schedule_time" class="form-control timepicker" value="">
														</div>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<th style="width:30%"><span data-toggle="tooltip" title="Lets you automatically repeat this message in intervals.">Repetition</span></th>
											<td>
												<div class="repetition-one">
													<span data-toggle="tooltip" title="Send this message only once.">One Time Message.</span>
													<button  type="button" class="btn btn-xs btn-primary change-repetition">Change</button>
												</div>
												<div class="repetition-more" style="display:none">
													<div class="form-group">
														<div class="row">
															<div class="col-sm-6 col-md-6">
																<select name="repitition_type" class="form-control repeat-select" id="repitition_type">
																	<option value="none">Do not Repeat</option>
																	<option value="daily">Repeat Daily</option>
																	<option value="weekly">Repeat Weekly</option>
																	<option value="monthly">Repeat Monthly</option>
																	<option value="quarterly">Repeat Quarterly</option>
																	<option value="yearly">Repeat Yearly</option>
																</select>
																<button  type="button" class="btn btn-xs btn-primary change-repetition">Remove repetition</button>
															</div>
															<div class="col-sm-6 col-md-6">
																<div class="daily">
																	<div class="input-group">
																		<span class="input-group-addon">
																			Repeat Every
																		</span>
																		<input type="number" class="form-control" id="daily_repitition" name="daily_repitition" placeholder="1">
																		<span class="input-group-addon">Day(s)</span>
																	</div>
																</div>

																<div class="weekly" style="display:none;">
																	<div class="input-group">
																		<span class="input-group-addon">
																			Repeat Every
																		</span>
																		<input type="number" class="form-control" id="weekly_repitition" name="weekly_repitition" placeholder="1">
																		<span class="input-group-addon">Weeks(s)</span>
																	</div>
																</div>

																<div class="monthly"  style="display:none;">
																	<div class="input-group">
																		<span class="input-group-addon">
																			Repeat Every
																		</span>
																		<input type="number" class="form-control" id="monthly_repitition" name="monthly_repitition" placeholder="1">
																		<span class="input-group-addon">Month(s)</span>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="row" style="margin-top:20px;">
								<div class="col-sm-12 col-md-12">
									<input type="hidden" name="message_id" value="{{$message_id}}">
									<button type="submit" class="btn btn-primary pull-right">
										Next <i class="si si-arrow-right"></i> 
									</button>
								</div>
							</div>

						</fieldset>
					</div>


				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('scripts')

<script type="text/javascript">

	$(function(){

		$('.change-repetition').click(function(){

			$('#repitition_type').val('none');
			$('.repetition-one').toggle();
			$('.repetition-more').toggle();
		})

		$('.repeat-select').change(function(){

			let value = $('.repeat-select').val();

			if(value == 'daily'){

				$('.daily').show();
				$('.weekly, .monthly').hide();

			}else if(value == 'weekly'){

				$('.weekly').show();
				$('.daily, .monthly').hide();

			}else if(value == 'monthly'){

				$('.monthly').show();
				$('.daily, .weekly').hide();

			}else{

				$('.daily, .weekly, .monthly').hide();
			}
		})
	})

</script>


@endsection