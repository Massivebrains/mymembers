@extends('layouts.app')

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="javascript:void(0)">Home</a>
    <a class="breadcrumb-item" href="javascript:void(0)">Messages</a>
    <span class="breadcrumb-item active">All Messages</span>
</nav>

<h2 class="content-heading">
    <button type="button" class="btn btn-sm btn-primary d-md-none float-right ml-5" data-toggle="class-toggle" data-target=".js-inbox-nav" data-class="d-none d-md-block">Menu</button>
    <a href="{{url('message/new')}}" class="btn btn-sm btn-success float-right">
        <i class="fa fa-plus"></i> New Message
    </a>
    All Messages ({{count($messages)}})
</h2>
<div class="row">
    <div class="col-md-12 col-xl-12">

        <div class="block">
            <div class="block-header block-header-default">
                <div class="block-title">
                    <strong><strong>{{count($messages)}} Total Message(s)</strong>
                </div>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-toggle="block-option">
                        <i class="si si-arrow-left"></i>
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option">
                        <i class="si si-arrow-right"></i>
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                </div>
            </div>
            <div class="block-content">

                <div class="push">
                    <button type="button" class="btn btn-sm btn-secondary float-right">
                        <i class="fa fa-times text-danger mx-5"></i>
                        <span class="d-none d-sm-inline"> Delete</span>
                    </button>
                    <button type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-archive text-primary mx-5"></i>
                        <span class="d-none d-sm-inline"> Archive</span>
                    </button>
                    <button type="button" class="btn btn-sm btn-secondary">
                        <i class="fa fa-star text-warning mx-5"></i>
                        <span class="d-none d-sm-inline"> Star</span>
                    </button>
                </div>
                

                
                
                <table class="js-table-checkable table table-hover table-vcenter">
                    <tbody>
                        @foreach($messages as $row)
                        <tr>
                            <td class="text-center" style="width: 40px;">
                                <label class="css-control css-control-primary css-checkbox">
                                    <input type="checkbox" class="css-control-input">
                                    <span class="css-control-indicator"></span>
                                </label>
                            </td>
                            <td class="d-none d-sm-table-cell font-w600" style="width: 140px;">{{ucfirst($row->status)}}</td>
                            <td>
                                @if($row->status == 'pending')
                                <a class="font-w600" href="{{url('message/new/'.$row->id.'/'.md5(time()))}}">Scheduled for {{_getMessageReciepientsCount($row->id)}} contact(s)</a>
                                @else
                                <span class="font-w600">Sent to {{_getMessageReciepientsCount($row->id)}} contact(s)</span>
                                @endif
                                <div class="text-muted mt-5">
                                    {{$row->body}}
                                </div>
                            </td>
                            <td class="d-none d-xl-table-cell font-w600 font-size-sm text-muted" style="width: 120px;">
                                {{_date($row->created_at, true)}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
        </div>
        
    </div>
</div>

@endsection
