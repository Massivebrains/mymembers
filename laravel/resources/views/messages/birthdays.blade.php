@extends('layouts.app')

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="javascript:void(0)">Home</a>
    <span class="breadcrumb-item active">Birthdays</span>
</nav>


<div class="row gutters-tiny invisible" data-toggle="appear">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Bithdays</h3>
            </div>
            <div class="block-content">

                <div class="table-responsive">
                    <table class="table table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th>Month</th>
                                <th>Birthdays</th>
                                <th>Celebrated</th>
                                <th>Remaining</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($birthdays as $row)
                            <tr>

                                <td>{{$row['month']}}</td>
                                <td>{{$row['birthdays']}} contact(s)</td>
                                <td>{{$row['celebrated']}} contact(s)</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-primary"  data-toggle="tooltip" title="View contacts celebrating in January">
                                            <i class="fa fa-eye"></i> View contacts
                                        </button> 
                                        <button type="button" class="btn btn-sm btn-primary"  data-toggle="tooltip" title="Customize birthday message sent to for January, and much more...">
                                            <i class="fa fa-cogs"></i> Settings
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection