@extends('layouts.app')

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="javascript:void(0)">Home</a>
    <span class="breadcrumb-item active">Help Desk</span>
</nav>


<div class="row gutters-tiny invisible" data-toggle="appear">
    <div class="col-12">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Help Desk</h3><br>
                <p>Having issues or questions? Get in touch with us.</p>

            </div>
            <div class="block-content">
             
                <div class="row">
                    <div class="col-6 col-md-4 col-xl-2">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="mt-5">
                                    <i class="fa fa-phone fa-4x"></i>
                                </p>
                                <p class="font-w600">+2349060051239</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-6 col-md-4 col-xl-2">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="mt-5">
                                    <i class="fa fa-whatsapp fa-4x"></i>
                                </p>
                                <p class="font-w600">+2348175020329</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-6 col-md-4 col-xl-2">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="mt-5">
                                    <i class="fa fa-envelope-open fa-4x"></i>
                                </p>
                                <small class="font-w600">vadeshayo@gmail.com</small>
                            </div>
                        </a>
                    </div>

                    <div class="col-6 col-md-4 col-xl-2">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="mt-5">
                                    <i class="fa fa-twitter fa-4x"></i>
                                </p>
                                <p class="font-w600">@massivebrains00</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="mt-5">
                                    <i class="fa fa-globe fa-4x"></i>
                                </p>
                                <p class="font-w600">stribe.tft-spark.co</p>
                            </div>
                        </a>
                    </div> 
                    <div class="col-6 col-md-4 col-xl-2">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="mt-5">
                                    <i class="fa fa-google-plus fa-4x"></i>
                                </p>
                                <p class="font-w600">@massivebrains</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection