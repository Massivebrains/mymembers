@extends('layouts.app')

@section('content')
<div class="row gutters-tiny">

    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-bubble fa-3x text-body-bg-dark"></i>
                </div>
                 <div class="font-size-h3 font-w600">
                    <span data-toggle="countTo" data-speed="1000" data-to="5.3">0</span>
                </div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">SMS Balance</div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-paper-plane fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600">
                    <span data-toggle="countTo" data-speed="1000" data-to="{{$sent}}">0</span>
                </div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    Sent
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-cloud-upload fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600">
                    <span data-toggle="countTo" data-speed="1000" data-to="{{$queued}}">0</span>
                </div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    Queued Messages
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-users fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600">
                    <span data-toggle="countTo" data-speed="1000" data-to="{{$contacts}}">0</span>
                </div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    contacts
                </div>
            </div>
        </a>
    </div>

</div>


<div class="row gutters-tiny invisible" data-toggle="appear">
    <div class="col-12">
        <div class="block">
            <div class="block-content">
                <div class="row items-push">
                    <div class="col-12">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    
    $(() => {

        $('#calendar').fullCalendar({

            header : {

                left : 'prev, next, today',
                center : 'title',
                right: 'month, agendaWeek, agendaDay, listMonth'
            },
            defaultDate : '{{date('Y-m-d')}}',
            navLinks : true,
            businessHours : false,
            editable : false,
            droppable : false,
            events : [

                @foreach($birthdays as $row)

                {
                    title : '{{$row['title']}}',
                    start : '{{$row['start']}}',
                    allDay : true,
                    url : '#',
                    color : '#FFEB3B'
                },
                @endforeach
            ]
        })
    })
</script>
@endsection