<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Stribe Landing page Template">
    <title>Stribe Landing page Template</title>
    <link rel="stylesheet" href="{{asset('public/landing')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('public/landing')}}/css/elegant-icons.css">
    <link rel="stylesheet" href="{{asset('public/landing')}}/css/animate.css">
    <link rel="stylesheet" href="{{asset('public/landing')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('public/landing')}}/css/responsive.css">
    <link rel="shortcut icon" href="{{asset('public/landing')}}/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{asset('public/landing')}}/images/favicon.png" type="image/x-icon">
    <script src="{{asset('public/landing')}}/js/modernizr.js"></script>
</head>


<body data-spy="scroll" data-target=".navbar-default" data-offset="100">


    <header>

        <div class="navbar navbar-default navbar-fixed-top affix-top" role="navigation" data-spy="affix" data-offset-top="50">

            <div class="container">



                <div class="navbar-header">

                    <button type="button" class="navbar-toggle dropdown-button waves-effect waves-teal btn-flat" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>

                    </button>
                    

                    <a class="navbar-brand logo" href="index.html">
                        <img src="{{asset('public/landing')}}/images/logo-white.png">
                    </a>
                </div>

                
                <div class="navbar-collapse collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right navigation-links">


                        <li class="connect-links">

                            @if(Auth::check())
                                <a href="{{url('home')}}" class="white-btn">Dashboard</a>
                            @else
                                <a href="{{url('register')}}" class="white-btn">Sign up</a>
                                <a href="{{url('login')}}" class="btn-login">Login</a>
                            @endif
                            

                        </li>

                    </ul>
                </div>

            </div>

        </div>

    </header>
    

    

    <section id="home" class="section-hero bg-color-1">


        <div class="section-hero-inner ">
            <div class="section-hero-content clearfix ">
                <div class="content-col-left ">

                    <div class="content-col-inner ">
                        <div class="hero-text ">
                            <h1>Engage People via SMS Seamlessly </h1>

                            <p>Send and Schedule Multi Lingua SMS and more</p>
                            <a href="index.html# " class="btn btn-white "> Get started</a>
                        </div>
                        
                  </div>
              </div>

              <div class="content-col-right ">

                <div class="content-col-image ">
                    <figure class="image ">
                        <img src="{{asset('public/landing')}}/images/image1.png" alt="">

                        <a href="#" class="play-btn">
                          <img src="{{asset('public/landing')}}/images/btn-play.png" class="img-responsive hidden-xl">
                      </a>


                  </figure>

              </div>
          </div>


      </div>
  </div>


</section>


<script src="{{asset('public/landing')}}/js/jquery-1.12.4.min.js"></script>
<script src="{{asset('public/landing')}}/js/bootstrap.min.js"></script>
<script src="{{asset('public/landing')}}/js/jquery.easing.js"></script>
<script src="{{asset('public/landing')}}/js/wow.min.js"></script>
<script src="{{asset('public/landing')}}/js/jquery.scrollUp.min.js"></script>
</body>